#four nested loops. the 1..n sets the bounds and the step, the |a| defines the variable
#powers get really large, use ruby because even unsigned long long int will fail us here
#ruby has support for integers larger than 64 bits by chaining together sequences of quad words
#with a particular flag
#when these ints hit 9223372036854775807, we're gonna see some serious ****
(3..100).each do |n|
  (1..n).each do |a|
    (1..n).each do |b|
      (1..n).each do |c|
        if ( a**n + b**n == c**n )
          print "you broke it, idiot"
          exit
        end
      end
    end
  end
end
print "still works"
#the time complexity is atrocious, it's O(n^4). While the inner loops only go up to the current iteration of the outer loop, they eventually become body traversing
