require 'pry'
class Freq

  def initialize(filename)
    #open the file, then read it
    inFile = File.open(filename, "r")
    #split the file into an array of string, delimited by spaces
    @text = inFile.read.split(" ")
    #take the array of string(binary ints), and convert them to base 10
    @text.map! do |string|
      string.to_i(2)
    end
  end

  def output
    @count = {}
    @text.each do |number|
      #do this to keep count of the occurences of certain 'letters'
      if @count.has_key?(number)
        @count[number] += 1
      else
        @count[number] = 1
      end
    end
    #now sort them, according to the value
    #print it, convert it to a hash then a string, to avoid a bunch of new lines
    outFile = File.open("freq_out.txt","w")
    outFile << @count.sort_by{ |k,v| -v}.to_h.to_s

  end

end
pry
