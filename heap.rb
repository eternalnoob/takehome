#modify the array class so that we get the dynamic typing features baked in
require 'pry'
class Array
  def heapSort
    self.heapify
    last = self.length - 1

    while( last > 0 )
      #swap 'last' and 'first' elements
      self[last], self[0] = self[0], self[last]
      last -= 1
      self.siftDown(0,last)
    end
    self
  end
    def heapify
      first = (self.length-2)/2

      while first >= 0
        self.siftDown(first, self.length-1)
        first -=1
      end
      self
    end

    def siftDown(first, last)
      root = first
      while ( root * 2 + 1 <= last )
        child = root*2+1
        swap = root

        if self[swap] < self[child] then
          swap = child
        end

        if (child+1 <= last && self[swap] <self[child+1])
          swap = child + 1
        end
        if ( swap != root)
          self[root],self[swap]=self[swap],self[root]
          root=swap
        else
          return
        end
      end
    end
end
pry
