#include <cstdlib>
//drawing has letters, this has ints, choose ints
struct datanode {
    int data;
    datanode * next;
};

struct treenode {
    datanode * next;
    treenode * left;
    treenode * right;
};

treenode * root;

treenode * search( int key, treenode * node)
{
    //
    if (node->next->data == key)
        return node;
    else if ( key < node->next->data )
        return search(key, node->left);
    else
        return search(key, node->right);
};
int find(int key)
{
    treenode * node = search(key, root);
    if (node == NULL)
        return 0;
    else
    {
        int count = 1;
        datanode *nextnode = node->next;
        while ( nextnode != NULL )
        {
            count++;
            nextnode = nextnode->next;
        }
        return count;
    }
};
