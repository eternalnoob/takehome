require 'pry'
class PolyCyph

  def initialize( filename )
    inFile = File.open(filename, "r")
    #forgive the use of regex here, we're removing all whitespace to make
    #it harder to look for individual words in the enciphered text
    #ensure the program only deals with alphabet chars
    @unmodified = inFile.read.gsub!(/\s+/,"").downcase.gsub!(/[^a-z]/,"")
    #generate a 576 long hash, containing all possible new mappings, and the letter they will correspond to
    count = 0
    @charhash = {}
    ('a'..'z').each do |char1|
      ('a'..'z').each do |char2|
        @charhash[char1+char2] = count
        count+=1
      end
    end
    p @unmodified
    p @charhash
    process()
  end


  private

  def process()
    #this is words in a double byte sense
    @words = []
    @unmodified.chars do 
      temp = @unmodified.slice!(/[a-z]{2}/)    
      if temp.class == String then
        @words << temp
      end
    end
    outFile = File.open("output.txt", "w")
    @words.each do |word|
      outFile << @charhash[word].to_s(2) << ' '
    end
    outFile.close
  end
end
pry
